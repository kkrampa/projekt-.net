﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace projekt.Models
{
    public class CartModel
    {
        public int Id { get; set; }

        public int OrderId { get; set; }

        public int BookId { get; set; }

        [ForeignKey("BookId")]
        public virtual BookModel Book { get; set; }

        public int Count { get; set; }

    }
}