﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace projekt.Models
{
    public class OrderModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public DateTime DateOfOrder { get; set; }

        public ICollection<CartModel> CartModels
        {
            get
            {
                ApplicationDbContext dbContext = new ApplicationDbContext();
                return dbContext.CartModels.Where(c => c.OrderId == Id).ToList();
            }
        }

        [NotMapped]
        public decimal Price {
            get
            {
                ApplicationDbContext dbContext = new ApplicationDbContext();
                return dbContext.CartModels.Where(x => x.OrderId == Id).Sum(x => x.Count * x.Book.Price);
            } 
        }
    }
}