﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace projekt.Models
{
    public class UserViewModel
    {
        public UserViewModel() { }

        public UserViewModel(ApplicationUser user)
        {
            Id = user.Id;
            Login = user.UserName;
            Roles = user.Roles.ToList().Select(r => r.Role.Name);
        }

        public string Id { get; set; }

        public string Login { get; set; }

        public IEnumerable<string> Roles { get; set; } 
    }
}