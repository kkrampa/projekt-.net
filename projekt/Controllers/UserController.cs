﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using projekt.Models;
using WebGrease.Css.Extensions;

namespace projekt.Controllers
{
    public class UserController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private UserManager<ApplicationUser> um =
            new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
        private IdentityManager imManager = new IdentityManager();

        // GET: /User/
        public ActionResult Index()
        {
            List<UserViewModel> users = new List<UserViewModel>();
            foreach (var user in db.Users)
            {
                users.Add(new UserViewModel(user));
            }
            return View(users);
        }

        // GET: /User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        // GET: /User/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = um.FindById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            List<SelectListItem> roles = new List<SelectListItem>();
            foreach (IdentityRole role in db.Roles)
            {
                roles.Add(new SelectListItem
                {
                    Text = role.Name,
                    Value = role.Id,
                    Selected = user.Roles.ToList().Find(r => r.Role.Name == role.Name) != null
                });
            }

            ViewBag.Roles = roles;
            return View(new UserViewModel(user));
        }

        // POST: /User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Login,Roles")] UserViewModel userviewmodel)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser au = um.FindById(userviewmodel.Id);
                imManager.ClearUserRoles(userviewmodel.Id);
                userviewmodel.Roles.ForEach(r => imManager.AddUserToRole(userviewmodel.Id, imManager.GetRoleById(r).Name));
                au.UserName = userviewmodel.Login;
                return RedirectToAction("Index");
            }
            return View(userviewmodel);
        }

        // GET: /User/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = um.FindById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(new UserViewModel(user));
        }

        // POST: /User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser au = um.FindById(id);
            var user = db.Users.First(u => u.Id == id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
