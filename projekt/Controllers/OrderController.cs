﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using projekt.Models;

namespace projekt.Controllers
{
    public class OrderController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Order/
        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            var ordermodels = db.OrderModels.Include(o => o.User);
            return View(ordermodels.ToList());
        }

        public ActionResult MyOrders()
        {
            var id = User.Identity.GetUserId();
            var ordermodels = db.OrderModels.Include(o => o.User).Where(o => o.UserId == id);
            return View("Index", ordermodels.ToList());
        }

        // GET: /Order/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderModel ordermodel = db.OrderModels.Find(id);
            if (ordermodel == null)
            {
                return HttpNotFound();
            }
            return View(ordermodel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
