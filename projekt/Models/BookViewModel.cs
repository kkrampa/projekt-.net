﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projekt.Models
{
    public class BookViewModel
    {
        public BookViewModel() { }

        public BookViewModel(BookModel bookModel)
        {
            Id = bookModel.Id;
            Title = bookModel.Title;
            Description = bookModel.Description;
            AuthorId = bookModel.AuthorId;
            CategoryId = bookModel.CategoryId;
            ImageUrl = bookModel.ImageUrl;
            Isbn = bookModel.Isbn;
            Price = bookModel.Price;
        }
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public int AuthorId { get; set; }

        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        [ForeignKey("AuthorId")]
        public virtual AuthorModel Author { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }

        public string Isbn { get; set; }

        [Range(1, 100)]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        public IEnumerable<SelectListItem> Authors
        {
            get
            {
                ApplicationDbContext db = new ApplicationDbContext();
                return db.AuthorModels.Select(a => new SelectListItem
                {
                    Text = a.LastName,
                    Value = a.Id.ToString(),
                });
            }
        }

        public IEnumerable<SelectListItem> Categories
        {
            get
            {
                ApplicationDbContext db = new ApplicationDbContext();
                return db.Categories.Select(c => new SelectListItem
                {
                    Text = c.Name,
                    Value = c.Id.ToString(),
                });
            }
        }
    }
}