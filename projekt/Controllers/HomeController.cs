﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using projekt.Models;

namespace projekt.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            ViewBag.Categories = db.Categories;
            return View();
        }

        public ActionResult BookList(int categoryId)
        {

            return View(db.BookModels.Where(b => b.CategoryId == categoryId).ToList().Select(b => new BookViewModel(b)));

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}