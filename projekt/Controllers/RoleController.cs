﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using projekt.Models;

namespace projekt.Controllers
{
    public class RoleController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private RoleManager<IdentityRole> rm = 
                new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
        private IdentityManager imManager = new IdentityManager();

        // GET: /Role/
        public ActionResult Index()
        {
            ApplicationDbContext dbContext = new ApplicationDbContext();
            List<RoleViewModel> roles = new List<RoleViewModel>();
            foreach (IdentityRole role in dbContext.Roles)
            {
                roles.Add(new RoleViewModel(role));
            }
            return View(roles);
        }

        // GET: /Role/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole role = rm.FindById(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(new RoleViewModel(role));
        }

        // GET: /Role/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Role/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] RoleViewModel roleviewmodel)
        {
            if (ModelState.IsValid)
            {
                imManager.CreateRole(roleviewmodel.Name);
                return RedirectToAction("Index");
            }

            return View(roleviewmodel);
        }

        // GET: /Role/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole role = rm.FindById(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(new RoleViewModel(role));
        }

        // POST: /Role/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] RoleViewModel roleviewmodel)
        {
            if (ModelState.IsValid)
            {
                IdentityRole role = rm.FindById(roleviewmodel.Id);
                role.Name = roleviewmodel.Name;
                rm.Update(role);
                return RedirectToAction("Index");
            }
            return View(roleviewmodel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
