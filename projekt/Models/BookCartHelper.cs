﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projekt.Models
{
    public class BookCartHelper
    {
        public BookCartHelper(BookModel book)
        {
            Book = book;
            Count = 1;
        }
        public BookModel Book { get; set; }

        public int Count { get; set; }
    }
}