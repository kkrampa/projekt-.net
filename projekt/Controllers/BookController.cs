﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using projekt.Models;

namespace projekt
{
    [Authorize(Roles = "admin")]
    public class BookController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Book/
        public ActionResult Index()
        {
            return View(db.BookModels.ToList());
        }

        [AllowAnonymous]
        // GET: /Book/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookModel bookmodel = db.BookModels.Find(id);
            if (bookmodel == null)
            {
                return HttpNotFound();
            }
            return View(bookmodel);
        }

       

        // GET: /Book/Create
        public ActionResult Create()
        {
            return View(new BookViewModel());
        }

        // POST: /Book/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Title,Description,AuthorId,CategoryId,ImageUrl,Isbn,Price, CopiesCount")] BookViewModel bookmodel)
        {
            if (ModelState.IsValid)
            {
                BookModel book = new BookModel(bookmodel);
                db.BookModels.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bookmodel);
        }

        // GET: /Book/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookModel bookmodel = db.BookModels.Find(id);
            if (bookmodel == null)
            {
                return HttpNotFound();
            }
            return View(new BookViewModel(bookmodel));
        }

        // POST: /Book/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Title,Description,AuthorId,CategoryId,ImageUrl,Isbn,Price")] BookViewModel bookviewmodel)
        {
            if (ModelState.IsValid)
            {
                BookModel bookmodel = new BookModel(bookviewmodel);
                db.Entry(bookmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bookviewmodel);
        }

        // GET: /Book/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookModel bookmodel = db.BookModels.Find(id);
            if (bookmodel == null)
            {
                return HttpNotFound();
            }
            return View(bookmodel);
        }

        // POST: /Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookModel bookmodel = db.BookModels.Find(id);
            db.BookModels.Remove(bookmodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
