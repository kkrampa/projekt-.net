﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using projekt.Models;

namespace projekt.Controllers
{
    public class CartController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private UserManager<ApplicationUser> um =
            new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
        public ActionResult Index()
        {
            if (Session["cart"] == null)
            {
                Session["cart"] = new List<BookCartHelper>();
            }
            return View(Session["cart"] as List<BookCartHelper>);
        }

        [HttpPost]
        public ActionResult AddToCart(int bookId)
        {
            if (Session["cart"] == null)
            {
                Session["cart"] = new List<BookCartHelper>();
            }
            BookModel bookModel = db.BookModels.Find(bookId);
            List<BookCartHelper> bookList = Session["cart"] as List<BookCartHelper>;
            BookCartHelper book = bookList.Find(x => x.Book.Id == bookId);
            if (book != null)
            {

                book.Count++;
                
            }
            else
            {
                bookList.Add(new BookCartHelper(bookModel));
            }
            return PartialView("_CartPartial", Session["cart"] as List<BookCartHelper>);
        }

        public ActionResult GetCart()
        {
            if (!(Session["cart"] is List<BookCartHelper>))
            {
                return PartialView("_CartPartial", new List<BookCartHelper>());
            }
            return PartialView("_CartPartial", Session["cart"] as List<BookCartHelper>);
        }

        public ActionResult DeleteItem(int id)
        {
            List<BookCartHelper> books = Session["cart"] as List<BookCartHelper>;
            books.RemoveAt(id);
            return View("Index", books);
        }

        public ActionResult ComputePrice()
        {
            List<BookCartHelper> bookList = Session["cart"] as List<BookCartHelper> ?? new List<BookCartHelper>();
            return Json(new Dictionary<String, Decimal>
            {
                {"price", bookList.Sum(x => x.Count * x.Book.Price)}
            });
        }

        public ActionResult Confirm()
        {
            OrderModel orderModel = new OrderModel
            {
                UserId = User.Identity.GetUserId(),
                DateOfOrder = DateTime.Now
            };
            db.OrderModels.Add(orderModel);
            db.SaveChanges();
            List<BookCartHelper> bookList = Session["cart"] as List<BookCartHelper>;
            foreach(var book in bookList)
            {
                CartModel cartModel = new CartModel {OrderId = orderModel.Id, BookId = book.Book.Id, Count = book.Count};
                db.CartModels.Add(cartModel);
            }
            db.SaveChanges();
            Session["cart"] = new List<BookCartHelper>();
            return View(orderModel);
        }
	}
}