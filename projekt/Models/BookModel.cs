﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace projekt.Models
{
    public class BookModel
    {
        public BookModel() { }

        public BookModel(BookViewModel bookViewModel)
        {
            Id = bookViewModel.Id;
            Title = bookViewModel.Title;
            Description = bookViewModel.Description;
            AuthorId = bookViewModel.AuthorId;
            CategoryId = bookViewModel.CategoryId;
            ImageUrl = bookViewModel.ImageUrl;
            Isbn = bookViewModel.Isbn;
            Price = bookViewModel.Price;
        }
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int AuthorId { get; set; }

        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        [ForeignKey("AuthorId")]
        public virtual AuthorModel Author { get; set; }

        public string ImageUrl { get; set; }

        public string Isbn { get; set; }

        public decimal Price { get; set; }
    }
}