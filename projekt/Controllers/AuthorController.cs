﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using projekt.Models;


namespace projekt.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "admin")]
    public class AuthorController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Author/
        public ActionResult Index()
        {
            return View(db.AuthorModels.ToList());
        }

        // GET: /Author/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorModel authormodel = db.AuthorModels.Find(id);
            if (authormodel == null)
            {
                return HttpNotFound();
            }
            return View(authormodel);
        }

        // GET: /Author/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Author/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,FirstName,LastName")] AuthorModel authormodel)
        {
            if (ModelState.IsValid)
            {
                db.AuthorModels.Add(authormodel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(authormodel);
        }

        // GET: /Author/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorModel authormodel = db.AuthorModels.Find(id);
            if (authormodel == null)
            {
                return HttpNotFound();
            }
            return View(authormodel);
        }

        // POST: /Author/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,FirstName,LastName")] AuthorModel authormodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(authormodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(authormodel);
        }

        // GET: /Author/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorModel authormodel = db.AuthorModels.Find(id);
            if (authormodel == null)
            {
                return HttpNotFound();
            }
            return View(authormodel);
        }

        // POST: /Author/Delete/5
        [System.Web.Mvc.HttpPost, System.Web.Mvc.ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AuthorModel authormodel = db.AuthorModels.Find(id);
            db.AuthorModels.Remove(authormodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
